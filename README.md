# WebMark21
This project is a proof of concept of microservices application, developed for educational purposes
It is composed by:
- webmark21db: the raw MongoDB database containerized
- webmark21be: the backend developed in python using flask framework. It covers the API called by the frontend
- webmark21fe: this is the frontend application developed in VueJS

These 3 elements are linked together by the ***docker-compose.yml*** file.

The application is a markdown editor which relays on ***easymde*** *v.2.12.1* and uses ***axios*** for the API.
The notes taken by *WebMark21* are stored in the DB and the images are registered in the DB and stored in a volume. Also the DB is stored in a volume.

The current implementation loads the whole content of the application (as a SPA), it doesn't query the backend each time searching for the note.


> **N.B.:** This is still a proof of concept even if usable. Many features have to be developed

# TODO
This is a project done in the free-time so there is no roadmap but the furhter implementations will include:
- Export in epub
- Export in PDF
- Image management
- User login
- User right management



