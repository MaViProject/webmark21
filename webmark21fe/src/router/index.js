import Vue from 'vue'
import VueRouter from 'vue-router'
import NoteBook from '../views/NoteBook.vue'
import Note from '../views/Note.vue'
import NotFound from '../views/NotFound.vue'

//import Userlist from '../views/Userlist.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: NoteBook
  },
 
  //notes
  {
    path: '/notebook',
    name: 'NoteBook',
    component: NoteBook
  },

  {
    path: '/note',
    name: 'Note',
    component: Note,
    props: true
  },

  {
    path: '/404',
    name: '404',
    component: NotFound,
    props: true
  },
  
  {
    path: '*',
    redirect: { name: '404', params: { resource: 'page' } }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
