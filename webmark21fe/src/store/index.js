import Vue from 'vue'
import Vuex from 'vuex'
import * as notes from '@/store/modules/notes.js'
import * as images from '@/store/modules/images.js'
import * as notifications from '@/store/modules/notifications.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    notes,
    images,
    notifications
  },
  state: {
  },
  mutations: {
  },
  actions: {
  },
  getters: {
  }
})
