import NoteService from '@/services/noteservices.js'
import i18n from '@/plugins/i18n.js'
import {convertBase64toBlob} from '@/plugins/helpers.js'


const lng = i18n.t('note_storage')
export const namespaced = true

export const state = {
    notes: [],
    current_note_id: ''
} 


export const mutations = {
    ADD_NOTE(state, note) {
      state.notes.push(note)      
    },
    SET_NOTE(state, notes){
      state.notes=notes
    },
    UPD_NOTE(state, note){
      //search the note to update in the state.notes array to get the pos
      let noteToChange = state.notes.filter(stnote =>{
        return stnote._id===note._id
      })[0]
      //find the position of the note to change in the state.notes array
      let i_note = state.notes.indexOf(noteToChange)
      //console.log('posizione della nota da cambiare '+i_note)
      //update the note
      state.notes[i_note]=note
    },
    DEL_NOTE(state, noteid){
      state.notes = state.notes.filter(
        note => note._id !== noteid
      )
    },
    UPD_TOC(state, note){
      //search the note to update in the state.notes array to get the pos
      let noteToChange = state.notes.filter(stnote =>{
        return stnote._id===note._id
      })[0]
      //find the position of the note to change in the state.note array
      let i_note = state.notes.indexOf(noteToChange)
      //update the note
      state.notes[i_note].toc=note.toc
    },
    SET_CURRENT_NOTE_ID(state,noteid){
      state.current_note_id = noteid
    }
}

export const actions = {
  fetchNotes({ commit, dispatch }){
      return NoteService.getNotes()
        .then(response => {
          commit('SET_NOTE', response.data)
        })
        .catch(error => {
          const notification = {
            type: 'error',
            message: lng.noteFetchFailure+': ' + error.message
          }
          dispatch('notifications/add', notification, { root: true })
          throw error
        })
    },

    delNote({commit, dispatch}, note) {
      return NoteService.delNote(note)
        .then(response => {
          console.log(response.data)
          commit('DEL_NOTE', note._id)
          const notification = {
            type: 'success',
            message: lng.noteDeletionSuccess
          }
          dispatch('notifications/add', notification, {root: true})
        })
        .catch(error => {
          const notification = {
            type: 'error',
            message: lng.noteDeletionFailure + ': ' + error.message
          }
          dispatch('notifications/add', notification, { root: true })
            throw error
        })
    },

    addNote({commit, dispatch}, note) {
      return NoteService.addNote(note)
        .then(response => {
          console.log("data from add response ")
          console.log(response.data)
          note._id = response.data.noteId
          console.log("noteId from action addNote: "+note._id)
          commit('ADD_NOTE', note)
          commit('SET_CURRENT_NOTE_ID', note._id)
          const notification = {
            type: 'success',
            message: lng.noteCreationSuccess
          }
          dispatch('notifications/add', notification, { root: true })
        })
        .catch(error => {
          const notification = {
            type: 'error',
            message: lng.noteCreationFailure + ': ' + error.message
          }
          dispatch('notifications/add', notification, { root: true })
            throw error
        })
    },

    updNote({commit, dispatch}, note) {
      return NoteService.updNote(note)
        .then(() => {
          commit('UPD_NOTE', note)
          const notification = {
            type: 'success',
            message: lng.noteCreationSuccess
          }
          dispatch('notifications/add', notification, { root: true })
        })
        .catch(error => {
          const notification = {
            type: 'error',
            message: lng.noteCreationFailure + ': ' + error.message
          }
          dispatch('notifications/add', notification, { root: true })
            throw error
        })
    },

    downloadNote({dispatch},note){
      console.log("downloading the note")
      return NoteService.dwnldNote(note)
      .then((received) => {
        //console.log("Response from download")
        //console.log(received)
        if(received.data.status!=200){
          console.log('error from server')
          alert(lng.noteDownloadError)
          const notification = {
            type: 'error',
            message: lng.noteDownloadError
          }
          dispatch('notifications/add', notification, { root: true })
        }
        else {
          //console.log('returned compressed file')
          let blob = convertBase64toBlob(received.data.data)
          const link = document.createElement('a')
          link.download = received.data.filename
          link.href = window.URL.createObjectURL(blob)
          link.click()
          URL.revokeObjectURL(link.href)
          //notification
          alert(lng.noteDownloadSuccess)
          const notification = {
            type: 'success',
            message: lng.noteDownloadSuccess
          }
          dispatch('notifications/add', notification, { root: true })
          //succesfully downloaded, remove the zip file from the server
          NoteService.dwnldClean({"filename":received.data.filename})
        }
      })
      .catch(error => {
        alert(lng.noteDownloadError)
        const notification = {
          type: 'error',
          message: lng.noteDownloadError + ': ' + error.message
        }
        dispatch('notifications/add', notification, { root: true })
          throw error
      })
    },

    //only in the store, it's updated with the while saving or updating the note
    storeNote({commit}, note) {
      commit('ADD_NOTE', note)
    },
    storeUpdNote({commit}, note) {
      commit('UPD_NOTE', note)
    },
    updTOC({commit}, note) {
      commit('UPD_TOC', note)
    },
    setCurrentNoteId({commit},noteid){
      console.log("current note id from action: " +noteid)
      commit('SET_CURRENT_NOTE_ID',noteid)
    },
    

/*
    fetchBooks({ commit, dispatch, state }, { page }) {
        return BookService.getBooks(state.perPage, page)
          .then(response => {
            commit('SET_BOOKS_TOTAL', parseInt(response.headers['x-total-count']))
            commit('SET_BOOKS', response.data)
          })
          .catch(error => {
            const notification = {
              type: 'error',
              message: 'There was a problem fetching books: ' + error.message
            }
            dispatch('notifications/add', notification, { root: true })
          })
    },

    fetchBook({ commit, getters, state }, id) {
        if (id == state.book.id) {
          return state.book
        }
    
        var book = getters.getBookById(id)
    
        if (book) {
          commit('SET_BOOK', book)
          return book
        } else {
          return BookService.getBook(id).then(response => {
            commit('SET_BOOK', response.data)
            return response.data
          })
        }
    }*/
}

export const getters = {
  //return the whole book list
  getNotes: state => {
    return state.notes
  },

  //return the book identified by the id  
  getNoteById: state => id => {
      return state.notes.find(note => note._id === id)
  },

  //return the note where the word matches with title or authors or tags
  //in case of multiple matches the note is returned once due to the filter
  getNotesBySearch: state => word => {
    //console.log('searching by word '+word)
    //search by title
    let filtered = state.notes.filter(note => 
      (note.title).toLowerCase().indexOf((word).toLowerCase()) > -1 
    )

    //search by one fo the authors
    let by_auth = state.notes.filter(note => 
      (note.author.toString().toLowerCase().indexOf((word).toLowerCase()) > -1)
    )
    filtered = filtered.concat(by_auth.filter((item) => filtered.indexOf(item)<0))
    
    //search by one fo the tags
    let by_tag = state.notes.filter(note => 
      (note.tag.toString().toLowerCase().indexOf((word).toLowerCase()) > -1)
    )
    filtered = filtered.concat(by_tag.filter((item) => filtered.indexOf(item)<0))

    return filtered
  },

  //Working, the one before is to improve
  getCurrentNoteId: state => {
    console.log('calling current note id '+state.current_note_id)
    return state.current_note_id
  },
   
}



