import ImageService from '@/services/imageservices.js'
import i18n from '@/plugins/i18n.js'


const lng = i18n.t('image_storage')
export const namespaced = true

export const state = {
    image: {},
} 


export const mutations = {
    ADD_IMG(state, img) {
      state.image = img
    },
    SET_IMG(state, img){
      state.image=img
    },
    UPD_IMG(state, img){
      //search the note to update in the state.notes array to get the pos
      let imgToChange = state.images.filter(stimg =>{
        return stimg._id===img._id
      })[0]
      //find the position of the note to change in the state.notes array
      let i_img = state.images.indexOf(imgToChange)
      //console.log('posizione della nota da cambiare '+i_note)
      //update the note
      state.images[i_img]=img
    },
    DEL_IMG(state){
      state.image = {}
    },
    
}

export const actions = {
  uploadImg({commit, dispatch}, img){
    return ImageService.upldImg(img)
      .then(resp => {
        console.log('data from actions')
        console.log(resp.data)
        commit('ADD_IMG', resp.data)
        const notification = {
          type: 'success',
          message: lng.imageCreationSuccess
        }
        dispatch('notifications/add', notification, { root: true })
      })
      .catch(error => {
        const notification = {
          type: 'error',
          message: lng.imageCreationFailure + ': ' + error.message
        }
        dispatch('notifications/add', notification, { root: true })
        throw error
      })
  },

  deleteImg(){

  }
}
 

export const getters = {
  //return the whole img list
  getImage: state => {
    return state.image
  },
}



