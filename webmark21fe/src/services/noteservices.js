import axios from 'axios'

const apiClient = axios.create({
  //baseURL: `http://localhost:3000`, //used for local server testing
  baseURL: process.env.VUE_APP_BACKEND,
  

  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 10000
})

export default {
  getNotes() {    
    return apiClient.get('/getNotes')    
  },
  getNote(note) {
    return apiClient.post('/getNotes', note)
  },
  delNote(note) {
    return apiClient.post('/delNote',note)
  },
  addNote(note) {
    return apiClient.post('/addNote', note)
  },
  updNote(note) {
    return apiClient.post('/updNote', note)
  },
  dwnldNote(note) {
    return apiClient.post('/dwnldNote', note)
  },
  dwnldClean(filename) {
    return apiClient.post('/dwnldClean', filename)
  }

}
/*
 //for local server testing
export default {
  getNotes() {    
    return apiClient.get('/notes')    
  },
  getNote(note) {
    return apiClient.post('/notes/', note)
  },
  delNote(note) {
    return apiClient.delete('/notes/'+ note.id)
  },
  addNote(note) {
    return apiClient.post('/notes/', note)
  },
  updNote(note) {
    return apiClient.put('/notes/'+ note.id, note)
  }

}
*/