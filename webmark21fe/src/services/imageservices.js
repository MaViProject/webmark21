import axios from 'axios'

const apiClient = axios.create({
  //baseURL: `http://localhost:3000`,
  baseURL: process.env.VUE_APP_BACKEND,

  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 10000
})

export default {
  upldImg(img) {    
    return apiClient.post('/upldImg', img)    
  }

}

/*
export default {
  getNotes() {    
    return apiClient.get('/notes')    
  },
  getNote(note) {
    return apiClient.post('/notes/', note)
  },
  delNote(note) {
    return apiClient.delete('/notes/'+ note.id)
  },
  addNote(note) {
    return apiClient.post('/notes/', note)
  },
  updNote(note) {
    return apiClient.put('/notes/'+ note.id, note)
  }

}*/
