module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  pluginOptions: {
    i18n: {
      locale: process.env.VUE_APP_I18N_LOCALE, //'it',
      fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE, //'en',
      localeDir: 'locales',
      enableInSFC: true
    }
  },

  
}
