#!/usr/bin/env python3

'''
Deals with the note-related operations.
Uses the db initalization and creates a collection
Deals with the DB calling its operations to perform the note-related operations.
Use the helpers to validate and to log and deal with the token.
Use the settings for parametrization.
'''

from flask import jsonify, request, send_file, send_from_directory
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from db import db, getItem, setItem, updItem, remItem
from helpers.tokenizer import createToken
import helpers.standard_returns_msgs as rmsg
import settings
import helpers.logger as log
from helpers.validation import validate
import tempfile
import os
import shutil
import base64

logger = log.setlog("notes.log")

notes = db["Notes"]
images = db["Images"]

modelfile = settings.note_model


class AddNote(Resource):
  def post(self):
    logger.debug("addNote - addNote called using POST")
    postedData = request.get_json()
    
    #checks 
    '''
    nchk = nchecks('addNote', 'note_create', postedData)
    if nchk:
      logger.debug("addNotes - Impossible to continue the searching. Check nchecks log.")
      return jsonify(nchk)
    '''
    
    #checks if the note already exists
    try: 
      exists = checkNote({"title": postedData["title"]})
      if not exists:
        logger.debug('addNote - An error occurred while searching for the note existence. Check checkNote log and DB getItem log.')
        return jsonify(rmsg.m_500)
      elif exists and "status" in exists and exists["status"] != 404:
        logger.debug('addNote - An error occurred while searching for the note existence. Check checkNote log and DB getItem log.')
        return jsonify(exists)
      elif exists and "status" not in exists:
        logger.debug("addNote - The note already exists. Avoid duplicates. ")
        return jsonify(rmsg.m_403)
    except Exception as e:
      logger.error("addNote - An error occurred checking if the note to add already exists. " + str(e))
      return jsonify(rmsg.m_500)  
    
    #create the new note with posted data
    logger.debug('addNote - The note '+ postedData["title"] +' by ' + str(postedData["author"]) + " does not exist. It will be created")
    newNote = {
      "title": postedData["title"],
      "author": postedData["author"],
      #"first_modified": postedData["first_modified"],
      #"last_modified": postedData["last_modified"],
      "tag": postedData["tag"],
      "toc": postedData["toc"],
      "cover": postedData["cover"],
      "content": postedData["content"],
      "version": 1
    }

    #add the note
    try: 
      entering = setItem(notes, newNote)
      if entering:
        logger.info("addNote - new note added: " + postedData["title"] )
        logger.debug("New note id: "+entering)
        return jsonify({
          "status": 200,
          "message": "OK",
          "noteId": entering
        })
      else:
        logger.error("addNote - an error occurred while saving the data in DB. Check the DB setItem function log.")
        return jsonify(rmsg.m_500)
    except Exception as e:
      logger.error("addNote - error while enterning the new note: "+ str(e))
      return jsonify(rmsg.m_500)       


#-------------------------------------------------#


class GetNotes(Resource):
  def post(self):
    logger.debug("getNotes - get notes called using POST: get a single note")
    postedData = request.get_json()
    
    #checks 
    '''
    nchk = nchecks('getNotes', "note_search", postedData)
    if nchk:
      logger.debug("getNotes - Impossible to continue the searching. Check nchecks log.")
      return jsonify(nchk)
    '''
    try:
      nts = getItem(notes, postedData)
      if not nts:
        logger.debug("getNotes - No note found")
        return jsonify(rmsg.m_404)
      elif nts == "error":
        logger.error("getNotes - An error occurred while searching for notes. Check DB getItem log.")
        return jsonify(rmsg.m_500)
      return nts, 200
    except Exception as e:
      logger.error("Error while finding the notes: " + str(e))      
      return jsonify(rmsg.m_500)



  def get(self):
    logger.debug("getNotes - get books called using GET: get all the notes")
    #check
    '''
    nchk = nchecks('getNotes', 'note_search_all')
    if nchk:
      logger.debug("getNotes - Impossible to continue the searching. Check nchecks log.")
      return jsonify(nchk)
    '''
    logger.debug("getNotes - The logged user has the rigths to retrieve all the notes.")
    try:
      all_notes = getItem(notes)
      if all_notes: 
        logger.debug("getNotes - notes found")
        logger.debug(all_notes)
        return all_notes, 200
      else: 
        logger.debug("getNotes - No note has been found")
        return jsonify(rmsg.m_404)
    except Exception as e:
      logger.error("getNotes - Error while searching for notes: " + str(e))
      return jsonify(rmsg.m_500)

#-------------------------------------------------#


class DelNote(Resource):
  def post(self):
    logger.debug("delNote - delete note called using POST")
    postedData = request.get_json()
    
    #checks 
    '''
    nchk = nchecks('delNote', "note_delete", postedData)
    if nchk:
      logger.debug("delNote - Impossible to continue the searching. Check nchecks log.")
      return jsonify(nchk)
    '''
    logger.debug("delNote - The logged user has the rigths to delete the book.")
    noteId = postedData["_id"]
    logger.debug("delNote - Id to delete: "+ str(noteId))
    try:
      deleteNote = remItem(notes, str(noteId))
      if not deleteNote:
        logger.error("delNote - An error occurred while removing the note "+ noteId + ". Check the DB remItem log.")
        return jsonify(rmsg.m_500)
      logger.info("delNote - The note " + noteId + " has been removed.")
      return jsonify(rmsg.m_200)
    except Exception as e:
      logger.error("delNote - An error occurred while removing the note "+ noteId + ". " + str(e))
      return jsonify(rmsg.m_500)



#-------------------------------------------------#


class UpdNote(Resource):
  def post(self):
    logger.debug("updNote - update note called using PUT")
    postedData = request.get_json()
    
    #checks 
    '''
    nchk = nchecks('updNote', "note_delete", postedData)
    if nchk:
      logger.debug("updNote - Impossible to continue the searching. Check nchecks log.")
      return jsonify(nchk)
    '''
    noteId = postedData["_id"]
    logger.debug("updNote - Id to update: "+ str(noteId))

    # optimistic lock: if posted data version > db version then update
    pdversion = postedData["version"]
    version = checkVersion(noteId,pdversion)
    if not version:
      logger.error("updNote - Optimistic lock: The submitted version is lower than the db one. ")
      return rmsg.m_402
    else:
      try:
        for pd in postedData:
          if pd != "_id":
            updNote = updItem(notes, '_id', str(noteId), pd, postedData[pd])
            if not updNote:
              logger.error("updNote - An error occurred while updating the note "+ noteId + ". Check the DB updItem log.")
              return jsonify(rmsg.m_500)
        logger.info("updNote - The note " + noteId + " has been updated.")
        return jsonify(rmsg.m_200)
      except Exception as e:
        logger.error("updNote - An error occurred while updating the note "+ noteId + ". " + str(e))
        return jsonify(rmsg.m_500)

#-------------------------------------------------#

#---------- Download-Related functions

class DownloadNote(Resource):
  def post(self):
    logger.debug("dwnldNote - get notes called using POST: get a single note")
    postedData = request.get_json()
    logger.debug(postedData)
    noteid = postedData["_id"]
    try: 
      exists = checkNote({"_id":noteid})
      if not exists:
        logger.debug('dwnldNote - An error occurred while searching for the note existence. Check checkNote log and DB getItem log.')
        return jsonify(rmsg.m_500)
      elif exists and "status" in exists and exists["status"] != 404:
        logger.debug('dwnldNote - An error occurred while searching for the note existence. Check checkNote log and DB getItem log.')
        return jsonify(exists)
      elif exists and "status" not in exists:
        logger.debug("dwnldNote - The note exists. Write the content in a file")
        title = exists[0]["title"]
        logger.debug("dwnldNote - Note title: "+ title)
        content = exists[0]["content"]
        # create a folder to store file and information under /tmp in the backend server filesystem
        dwnldFolder = tempfile.mkdtemp(suffix=None,prefix=title+'_')
        if not os.path.exists(dwnldFolder):
          logger.error("dwnldNote - Temp download folder hasn't been created ")
          raise ValueError('Temp download folder has not been created')
        logger.debug("dwnldNote - Created a temporary folder for the content to download: "+dwnldFolder)
        #create a subfolder for images
        dwnldImgFolder = tempfile.mkdtemp(suffix=None,prefix='img_',dir=dwnldFolder)
        if not os.path.exists(dwnldImgFolder):
          logger.error("dwnldNote - Image subfolder in temp download folder has not been created")
          raise ValueError('Image subfolder in temp download folder has not been created')
        logger.debug("dwnldNote - Created a temporary image subfolder for the content to download: "+dwnldImgFolder)
        # replace the network path with a local img path in the text
        logger.debug("dwnldNote - Replacing the network path with a local img path in the text")
        #leaving only the name of the image subfolder not the full path for the file content
        imgSubFolder = dwnldImgFolder.replace(dwnldFolder+'/','')
        netpath = settings.backend_server + settings.storage[1:len(settings.storage)] #remove the dot and add the server address
        content = content.replace(']('+netpath,']('+imgSubFolder)

        '''
        #img and note in the same directory: remove the path leaving the image name
        netpath = settings.backend_server + settings.storage[1:len(settings.storage)] #remove the dot and add the server address
        content = content.replace(']('+netpath,'](')
        '''
        # write the modified content in an MD file
        with open(os.path.join(dwnldFolder, title+'.md'),"w") as text:
          logger.debug("dwnldNote - file opened")
          text.write(content)
          logger.debug("dwnldNote - file written")
          text.close()
          logger.debug("dwnldNote - file closed")
        #search for the images related to this noteid
        imgs = getItem(images,{"noteid":noteid})
        logger.debug("dwnldNote - retrieved the images related to the noteid")
        #logger.debug(imgs)
        #copy the images from the path to the temp dir
        for img in imgs:
          #logger.debug("dwnldNote - element in returned array")
          #logger.debug(img)
          src_img = os.path.join(settings.storage, img["name"])
          #dest_img = os.path.join(dwnldFolder, img["name"])
          dest_img = os.path.join(dwnldImgFolder, img["name"])
          shutil.copyfile(src_img, dest_img)
        #zip the content to send back and remove the folder
        #zipFileName = dwnldFolder.replace('/tmp/','')
        shutil.make_archive(dwnldFolder, settings.compression_type, dwnldFolder)
        logger.debug("dwnldNote - download content folder has been zipped in "+dwnldFolder)
        #remove the temp folder 
        shutil.rmtree(dwnldFolder)
        logger.debug("dwnldNote - download temp folder has been removed")
        '''
        # move files in static folder
        moved = shutil.move(dwnldFolder,settings.storage)
        moved = moved[2:len(moved)]
        logger.debug("dwnldNote - Download folder moved from temp to storage, new name: "+moved)
        return send_file(os.path.join(moved,title+'.md'),as_attachment=True)
        '''
        # set the extension for the compressed file according to the settings
        cmp_ext = ''
        if settings.compression_type=='zip':
          cmp_ext = '.zip'
        if settings.compression_type=='tar':
          cmp_ext = '.tar'
        if settings.compression_type=='gztar':
          cmp_ext = '.tar.gz'
        
        #return send_file(dwnldFolder+cmp_ext,as_attachment=True)

        #trying the encoded way
        compressedFilePath = dwnldFolder+cmp_ext
        encoded=''
        with open(compressedFilePath, 'rb') as binary_file:
          binary_file_data = binary_file.read()
          encoded = (base64.b64encode(binary_file_data)).decode('utf-8')
          logger.debug(encoded)
        filename = dwnldFolder.replace('/tmp/','')+cmp_ext
        return jsonify({
          "status":200,
          "filename": filename,
          "data": encoded
        })
          
    except Exception as e:
      logger.error("dwnldNote - An error occurred dealing with the note. " + str(e))
      return jsonify(rmsg.m_500)  

class CleanDownloaded(Resource):
  def post(self):
    logger.debug("dwnldClean - clean request called using POST")
    postedData = request.get_json()
    filename = postedData["filename"]
    logger.debug('The file to remove is: '+filename)
    try: 
      filepath = os.path.join('/tmp',filename)
      os.remove(filepath)
      logger.debug('dwnldClean - File '+filepath+' has been succesfully deleted.')
      return jsonify({
        'status': 200,
        'message': 'File '+filename+' has been removed.'
      })
    except Exception as e:
      logger.error("dwnldClean - An error occurred while trying to clean the /tmp folder removing the downloaded compressed file: "+str(e))
      return jsonify(rmsg.m_417_img)


#---------- Internal functions

# performs all the checks: 
# - if posted data is valid
def nchecks(action, postedData=None):
  if postedData is not None:
    valid = validate(postedData, action, modelfile)
    if not valid:
      logger.error("nchecks for " + action + " - Posted invalid data")
      return rmsg.m_400
  else:
    logger.debug("nchecks for "+ action + " - The function didn't return any error message, validation succesfully done.")
    return False


# check if the note exists
def checkNote(params):
  try:
    note = getItem(notes, params)
    if note and note != "error":
      logger.debug("checkNote - The searched note exists.")
      return note
    elif note and note == "error":
      logger.debug("checkNote - An error occurred searching fo the note. Check the DB getItem log.")
      return rmsg.m_500
    elif not note:
      logger.debug("checkNote - The searched note doesn't exist.")
      return rmsg.m_404
  except Exception as e:
    logger.error("checkNote - An error occurred while checking the note: " + str(e))
    return rmsg.m_500

# check the version
def checkVersion(noteId,posted_version):
  note = checkNote({"_id":noteId})
  if note and "status" not in note:
    if note[0]["version"]>=posted_version:
      logger.debug("checkVersion - The posted version is less recent than the DB one")
      return False
    else:
      return True
  else: 
    logger.error("checkVersion - An error occurred while checking the note version for noteid: " +posted_version)
    return False

