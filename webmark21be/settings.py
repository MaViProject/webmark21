# LOGGING
logfolder = "./logs/"
loglevel = "DEBUG"

'''
# JWT
jwt_header_name = "Authorization"
jwt_header_type = "Bearer"
jwt_secret = "jwtsecret" # must take it from the env
jwt_token_location = "headers"
jwt_access_token_expires = 86400 #in seconds: 1 day
#jwt_refresh_token_expires = 86400 #in seconds: 1 day
'''

# Server info
backend_server = "http://webmark21:5000"#""http://localhost:5000"#

# Img Data
img_max_size_MB = 2
allowed_images = ["JPEG", "JPG", "PNG", "GIF"]
storage = "./static/imageStorage"

# Download to local
compression_type = 'gztar' #allowed types: tar, gztar, zip


# DB Data Models
img_model = "./images/model.json"
note_model = "./notes/model.json"

# MONGO
mongo_host = "webmark21db" #is the docker-compose service for Mongo
mong_port = "27017"