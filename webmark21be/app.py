#!/usr/bin/env python3

import settings
from flask import Flask, render_template
from flask_restful import Api
from routes import  initialize_note_routes, initialize_image_routes
from flask_cors import CORS
# from helpers.tokenizer import init_token

app = Flask(__name__)
api = Api(app)


# just for localhost testing, disable in production
CORS(app)
@app.route("/")
def helloWorld():
  return "Hello, cross-origin-world!"


'''
@app.route("/imageStorage")
def strg():
  fx = '<img src="{{url_for(\'static\', filename=\'countrysideBackground.png\')}}" align="middle" />'
  return render_template(fx)

@app.route("/<path:filename>", methods=['GET'])
def send_file(filename):  
      return app.send_from_directory('/static', filename)
'''

# jwt = init_token(app)


initialize_note_routes(api)
initialize_image_routes(api)


if __name__=="__main__":
    app.run(host='0.0.0.0')
