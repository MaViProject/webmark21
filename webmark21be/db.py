#!/usr/bin/env python3

'''
Deals with the DB
It makes the application DB-independent: to change the DB let's change this file
It uses the helpers for the log
It initializes the DB using the settings 
'''

from pymongo import MongoClient
from bson import ObjectId
import settings
import json
import helpers.logger as log
logger = log.setlog("db.log")


client = MongoClient("mongodb://"+ settings.mongo_host +":" + settings.mong_port)
db = client.webmark21

'''
Receives the collection name
Recevies the field to use as item id and the value

#returns the results with field=value or False if it doesn't exist or error for any error
def existingItem(coll, field, value):
    logger.debug("existingItem - Search for " + field + " = " + value)
    try: 
        return coll.find({field: value})[0][field]
    except IndexError as e:
        logger.debug("existingItem - No results for " + field + " =  " + value + ": " + str(e) )
        return False
    except Exception as e:
        logger.error("existingItem - Error while finding " + field + " =  " + value + ": " + str(e) )
        return "error"
'''

# returns the results with field=value, exclude the object id and decode the pwd as a string
def getItem(coll, filters=None):
    logger.debug("getItem - Function called")
    logger.debug(filters)
    try:
        if filters is not None:          
            searchfor = ""
            logger.debug("getItem - Set the searching criteria for both the log string and the regex used for the value to search, as a like.")
            for filt in filters:
                if isinstance(filters[filt], list):
                    logger.debug("getItem - Setting the regex search for each list element.")
                    logger.debug(str(filt))
                    nlist = []
                    for el in filters[filt]:
                        el =  {'$regex': el}
                        nlist.append(el)
                    #newfilter =  '{"$in":' + str(nlist) +'}'
                    newdict = { "$in": filters[filt] }
                    #newfilter =  '{"$in":' + filters[filt] +'}'
                    #logger.debug('getItem - New list filter: ' + newdict)
                    filters[filt] = newdict          
                elif filt == '_id':
                    logger.debug("getItem - Searching for an _id so must be converted in an object.")
                    filters[filt] = ObjectId(filters[filt])
                elif not isinstance(filters[filt], int):
                    logger.debug("getItem - Setting the regex search for a single element which is not a number.")
                    filters[filt] = {'$regex': filters[filt]}
                logger.debug(filters[filt])
                searchfor = searchfor + str(filt)
    except Exception as e:
        logger.debug("getItem - An error occurred while preparing the filters for the search: "+ str(e))    
        return "error"
    try:
        #logger.debug(type(coll.find({field: value}))) 
        if filters is not None:
            logger.debug("getItem - searching for " + searchfor)
            res = coll.find(filters)
        else: 
            logger.debug("getItem - no parameter, search for all")
            #res = coll.find({},{'_id': False})
            res = coll.find({})
        logger.debug("getItem - Cursor returned from Mongo, let's convert in a list without byte type")
        found = []
        for cur in res:
            if "_id" in cur:
                cur["_id"] = str(cur["_id"])
            if "password" in cur:
                cur["password"] = cur["password"].decode(encoding="UTF-8")
            found.append(cur)
        res.close() # kill this cursor
        return found
    except Exception as e:
        if filters is not None:
            logger.debug("getItem - Error searching for " + searchfor)
        else: 
            logger.error("getItem - Error while finding the items: " + str(e) )
        return "error"

# insert
def setItem(coll, value):
    logger.debug("setItem - Entering a new value" )
    try:
        result = coll.insert_one(value)
        logger.debug("setItem - insert result")
        logger.debug(result.inserted_id)
        logger.debug("setItem - New entry added in the database")
        return str(result.inserted_id)
    except Exception as e:
        logger.error("setItem - An issue happened while entering a new value in the DB: " + str(e))
        return False

# update the item in the collection
def updItem(coll, itemid, idval, field, value):
    logger.debug("updItem - Update " + field + " to the value " + str(value) + " for the " + itemid +  " = " + idval)
    if itemid == '_id':
        idval = ObjectId(idval)
    try: 
        coll.update({itemid: idval}, {'$set': {field: value}})
        return True
    except Exception as e:
        logger.error("updItem - Failed to update " + field + " for " + idval + " due to " + str(e))
        return False

# remove the item from the collection
def remItem(coll, itemid):
    logger.debug("remItem - removing the entry")
    try: 
        logger.debug("id to delete: "+ str(itemid))
        res = coll.delete_one({'_id': ObjectId(itemid)})
        logger.debug("remItem - entry removed. " + str(res.deleted_count) )
        return True
    except Exception as e:
        logger.error("remItem - Impossible to remove the item "+ itemid +" due to " + str(e))
        return False

# return the entry version 
# def getVersion(itemid):
#    logger.debug("getVersion - returing the version for the note id: " + str(itemid))
#    try: 
 #       res


#### IMAGE MANAGEMENT
def uploadImg(coll, bimage):
    logger.debug("uploadImg - Upload "+bimage.name+" image to MongoDB")
    try:
        coll.insert(bimage) 
        logger.debug("uploadImg - uploaded succeeded for "+bimage.name)
        return True
    except Exception as e:
        logger.error(str(e))
        logger.error("uploadImg - Impossible to upload the image "+ bimage.name +" due to " + str(e))
        return False
