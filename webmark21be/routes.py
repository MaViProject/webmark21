#!/usr/bin/env python3

from notes.notes import AddNote, GetNotes, DelNote, UpdNote, DownloadNote, CleanDownloaded
from images.images import UpldImage, ShowImage, GetImage, DelImage


def initialize_note_routes(api):
  api.add_resource(AddNote, '/addNote')
  api.add_resource(GetNotes, '/getNotes')
  api.add_resource(DelNote, '/delNote')
  api.add_resource(UpdNote, '/updNote')
  api.add_resource(DownloadNote, '/dwnldNote')
  api.add_resource(CleanDownloaded, '/dwnldClean')
  

def initialize_image_routes(api):
  api.add_resource(UpldImage, '/upldImg')
  api.add_resource(ShowImage, '/<path:filename>')
  api.add_resource(GetImage, '/getImg')
  api.add_resource(DelImage, '/delImg')
  

