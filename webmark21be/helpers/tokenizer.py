'''
Implements the token settings
Implement the token functionalities: 
- initialization, called by the main app.py
- token creation
Writes its own specific log
'''

from flask_jwt_extended import JWTManager, create_access_token, create_refresh_token
import settings
import helpers.logger as log

logger = log.setlog("tokenizer.log")



def init_token(app):
    app.config['JWT_HEADER_NAME'] = settings.jwt_header_name
    app.config['JWT_HEADER_TYPE'] = settings.jwt_header_type
    app.config['JWT_SECRET_KEY'] = settings.jwt_secret
    app.config['JWT_TOKEN_LOCATION'] = settings.jwt_token_location
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = settings.jwt_access_token_expires
    #app.config['JWT_REFRESH_TOKEN_EXPIRES'] = settings.jwt_refresh_token_expires

    return JWTManager(app)

def createToken(id, type):
    logger.debug("createToken - Function called")
    try:
        if type == "access":
            logger.debug("createToken - Create a (fresh) access token for "+ id)
            return create_access_token(identity= id)
        elif type == "refresh":
            logger.debug("createToken - Create a (not-fresh) refresh token for "+ id)
            return create_refresh_token(identity= id)
        else:
            logger.error("createToken - The passed token type argument "+ type +" is invalid.")
            raise ValueError("An internal error occurred.")
    except Exception as e:
        logger.error("createToken - An error occurred while creating the Token for the user: "+ str(e))
        return False
