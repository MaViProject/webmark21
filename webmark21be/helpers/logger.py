#!/usr/bin/env python3

import os
import logging
import settings
from datetime import date

def setlog(lognm):
    logfolder = settings.logfolder
    if not os.path.exists(logfolder):
        os.makedirs(logfolder)
    logname = logfolder + setNameTimePrefix() + "_" + lognm
    loglevel = settings.loglevel
    logger = logging.getLogger(logname)
    hdlr = logging.FileHandler(logname)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    if loglevel == "DEBUG":
        logger.setLevel(logging.DEBUG)
    elif loglevel == "ERROR":
        logger.setLevel(logging.ERROR)
    else:
        logger.setLevel(logging.INFO)
    return logger

def setNameTimePrefix():
    today = date.today()
    d1 = today.strftime("%d%m%Y")
    return str(d1)