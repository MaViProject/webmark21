#!/usr/bin/env python3

'''
Deals with the image-related operations.
Uses the db initalization and creates a collection
Deals with the DB calling its operations to perform the image-related operations.
Use the helpers to validate and to log and deal with the token.
Use the settings for parametrization.
'''

from flask import jsonify, request
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from PIL import Image
from db import db, uploadImg, getItem, setItem
from helpers.tokenizer import createToken
import helpers.standard_returns_msgs as rmsg
import settings
import helpers.logger as log
import json
import os
import base64
from helpers.validation import validate



logger = log.setlog("images.log")

images = db["Images"]

modelfile = settings.img_model
    
class UpldImage(Resource):
  def post(self):
    logger.debug("upldImg - upldImg called using POST")
    logger.debug('upldImg- headers')
    logger.debug(request.headers)
    try: 
      #getting data from json object
      postedData = request.get_json()
      chkres = datacheck(postedData) 
      if not chkres[0]:
        return jsonify ({
          "error": chkres[1]
        })
      else:
        logger.debug('upldImg - sanity check passed get the data from the json')
        #imgname = postedData["name"]
        imgname = chkres[1] # check returns a sanitized name
        logger.debug("upldImg - image filename: "+imgname)
        imgtype = postedData["type"]
        logger.debug("upldImg - image type: "+imgtype)
        imgsize = postedData["size"]
        logger.debug("upldImg - image size: "+str(imgsize))
        noteid = postedData["noteId"]
        logger.debug("upldImg - id of the image related note: "+str(noteid))
         #check if an image with this name already exists and if there are not errors while searching
        imgExists = checkImageExists({"name":imgname})
        if not imgExists[0] or (imgExists[0] and (imgExists[1] != rmsg.m_500)):
          # if the image doesn't exist then write it on a file
          if not imgExists[0]:
            logger.debug("upldImg - The image doesn't exist so it must be written on file system")
            encodedimg = postedData["bdata"]
            #logger.debug(encodedimg)
            #remove the heading info to the base64 string
            encodedimg = encodedimg.replace('data:'+imgtype+';base64,', "")
            #decode the string retrieving the binary image
            imgBin = base64.b64decode(encodedimg)   
            filename = settings.storage+"/"+imgname
            with open(filename,"wb") as imgfile:
              #imgfile = open(filename,"wb")
              logger.debug("upldImg - file opened")
              imgfile.write(imgBin)
              logger.debug("upldImg - file written")
              imgfile.close()
              logger.debug("upldImg - file closed")
              #path string to the image
              filepath = settings.backend_server + filename[1:len(filename)] #remove the dot and add the server address
              logger.debug("upldImg - filepath: "+filepath)
          else:
            #filepath must be taken from the image search
            logger.debug("upldImg - img returned from the exists check:")
            logger.debug(imgExists[1]["filePath"])
            filepath = imgExists[1]["filePath"]

          # in any case the DB entry must be added to link image to the note 
          logger.debug("upldImg - New entry must be added to the DB to link image and note ")
          #Preparing data to store in the DB
          # convert in an object for the DB 
          objimg = {
            #"img": encodedimg, #encoded base64 image can be added, at the moment using the file written in a folder
            "name": imgname,
            "noteid": noteid,
            "type": imgtype,
            "filePath": filepath
          }
          # send to DB
          upld = setItem(images,objimg)
          if upld:
            logger.debug("upldImg - "+imgname+" image properly loaded in the DB with the id "+upld+", linked to the noteid "+noteid+", returning the path: "+filepath)
            #returning the image path and data
            return jsonify ({
              "filePath": filepath,
              "imgId": upld,
              "imgNoteId": noteid,
              "imgName": imgname
            })
          else:
            logger.error("upldImg - Error while uploading the image "+imgname+" in the DB. Check the uploadImg DB log.")
            return jsonify(rmsg.m_500)        
        else:
          return jsonify ({
            "error": imgExists[1]
          })

    except Exception as e:
      logger.error("upldImg - Error using the posted data from json: "+str(e))
      return jsonify ({
          "error": rmsg.m_413_img
        })


class ShowImage(Resource):
  def get(self, filename):
    logger.debug("showImage - requiring "+filename)
    logger.debug("showImage - from "+settings.storage)
    return self.send_from_directory(settings.storage, filename)

class GetImage(Resource):
  def post(self):
    pass

class DelImage(Resource):
  def post(self):
    pass




#-------------------------------------------------#


#---------- Internal functions

# sanitize the filename
def sanitizeString(input_string):
  logger.debug("sanitizeString - Sanitizing the string... ")
  output_string = ''
  for i in input_string:
    if i == '>':
      outchar = '&gt;'
    elif i == '<':
      outchar = '&lt;'
    elif i == '!':
      outchar = '&#33;'
    elif i == '"':
      outchar = '&#34;'   
    elif i == '$':
      outchar = '&#35;'
    elif i == '$':
      outchar = '&#36;'
    elif i == '%':
      outchar = '&#37;'
    elif i == '&':
      outchar = '&#38;'
    elif i == "'":
      outchar = '&#39;'
    elif i == '*':
      outchar = '&#42;'
    elif i == '?':
      outchar = '&#63;'
    elif i == '@':
      outchar = '&#64;'   
    else:
      outchar = i
      output_string += outchar
  logger.debug("sanitizeString - The sanitized string is: "+output_string)
  return output_string

# check the filetype
def checkFileType(allowed, type):
  logger.debug("checkFileType - Checking if the file type "+type+" is allowed")
  for atype in allowed:
    if atype.lower() == type.lower():
      return True
  return False

def checkFileSize(maxSize, size):
  logger.debug("checkFileSize - Checking if the file size "+str(size)+" is allowed")
  if maxSize >= size:
    return True
  return False

def checkImageExists(params):
  try:   
    img = getItem(images, params)
    logger.debug("checkImageExists - getItem returned values:")
    logger.debug(img)
    if img and img != "error":
      logger.debug("checkImageExists - The searched image exists.")
      logger.debug(img[0])
      return [True, img[0]]
    elif img and img == "error":
      logger.debug("checkImageExists - An error occurred searching fo the image. Check the DB getItem log.")
      return [True, rmsg.m_500]
    elif not img:
      logger.debug("checkImageExists - The searched image doesn't exist.")
      return [False, rmsg.m_416_img]
  except Exception as e:
    logger.error("checkImageExists - An error occurred while checking the image: " + str(e))
    return [True, rmsg.m_500]


def datacheck(postedData):
  sanitizedName = sanitizeString(postedData["name"])

  #check the type
  ftype = postedData["type"].replace('image/', '')
  if not checkFileType(settings.allowed_images, ftype):
    logger.error("datacheck - The image has a not allowed type: "+str(ftype))
    return [False, rmsg.m_400_img]
  
  #check the max size
  if not checkFileSize(settings.img_max_size_MB*1000*1000,postedData["size"]):
    logger.error("datacheck - The image is too big: "+str(postedData["size"])+' bytes')
    return [False, rmsg.m_415_img]

  #sanitize the image name
  return [True, sanitizedName]
  





#check if is an image allowed format

# check image size
#def checkImgSize(img):
#  if img_max_size_MB

# check if the note exists
'''
def checkNote(params):
  try:
    note = getItem(notes, params)
    if note and note != "error":
      logger.debug("checkNote - The searched note exists.")
      return note
    elif note and note == "error":
      logger.debug("checkNote - An error occurred searching fo the note. Check the DB getItem log.")
      return rmsg.m_500
    elif not note:
      logger.debug("checkNote - The searched note doesn't exist.")
      return rmsg.m_404
  except Exception as e:
    logger.error("checkNote - An error occurred while checking the note: " + str(e))
    return rmsg.m_500

# check the version
def checkVersion(noteId,posted_version):
  return True
'''
